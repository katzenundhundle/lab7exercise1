import 'dart:async';
import 'package:app_settings/app_settings.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_controller/google_maps_controller.dart';
import 'package:location/location.dart';
import 'package:permission_handler/permission_handler.dart' as permission_handler;


class GoogleMapView extends StatefulWidget {
  const GoogleMapView({ Key? key }) : super(key: key);

  @override
  State<GoogleMapView> createState() => _GoogleMapViewState();
}

class _GoogleMapViewState extends State<GoogleMapView> {
  final s = "Map 2";
  late GoogleMapsController controller;
  late StreamSubscription<CameraPosition> subscription;
  late CameraPosition position;


  late LatLng userPosition;
  Future<LatLng> findUserLocation() async {
              
               Location location = Location();
               LocationData userLocation;
               PermissionStatus hasPermission = await
                location.hasPermission();
               bool active = await location.serviceEnabled();
               if (hasPermission == PermissionStatus.granted && active) {
                   userLocation = await location.getLocation();
                   print("oke");
                  //  print('my location: '+userLocation.toString());
                   userPosition = LatLng(userLocation.latitude!,
                    userLocation.longitude!);
                    print("user location:" +userPosition.toString());
               } else {
                  permission_handler.Permission permission = permission_handler.Permission.locationWhenInUse;
                  await permission.request();
                   bool _serviceEnabled = await location.requestService();
                   if(_serviceEnabled){
                     print("oke");
                      userLocation = await location.getLocation();
                 
                      userPosition = LatLng(userLocation.latitude!,
                      userLocation.longitude!);
                      print("user location:" +userPosition.toString());
                   }
                   print(_serviceEnabled);
                  //  userPosition = LatLng(51.5285582, -0.24167);
               }
               return userPosition;
    }
  
  @override
  void initState() {
    super.initState();
     
      
      controller = GoogleMapsController(
      myLocationButtonEnabled: false,
      myLocationEnabled: true,
      initialCameraPosition: CameraPosition(
        target: const LatLng(37.42796133580664, -122.085749655962),
        zoom: 14.4746,
      ),
      onTap: (latlng) {
        Circle circle = Circle(circleId: CircleId("circle"));
        circle = Circle(
          circleId: CircleId(
            "ID:" + DateTime.now().millisecondsSinceEpoch.toString(),
          ),
          center: latlng,
          fillColor: Color.fromRGBO(255, 0, 0, 1),
          strokeColor: Color.fromRGBO(155, 0, 0, 1),
          radius: 5,
          onTap: () => controller.removeCircle(circle),
          consumeTapEvents: true,
        );

        controller.addCircle(circle);
      },
    );
    findUserLocation().then((value) {
      setState(() {
        controller.moveCamera( CameraUpdate.newCameraPosition(CameraPosition(target: value, zoom: 16)));
        
        
      });
      
    },);
    
    subscription = controller.onCameraMove$.listen((e) {
      setState(() {
        position = e;
      });
    });
  }

  @override
  void dispose() {
    subscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    
    return Column(
      children: <Widget>[
        Expanded(
          child: Stack(
            children: [
              GoogleMaps(
              controller: controller,
              ),

              IconButton(onPressed: (){
                findUserLocation().then((value) {
                  setState(() {
                  controller.animateCamera( CameraUpdate.newCameraPosition(CameraPosition(target: value, zoom: 16)));
                
                });
                });
              }, icon: Icon(Icons.my_location))
            ]),
        ),
        
      ],
    );
  }
}